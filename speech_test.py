from typing import Callable, Optional
import torch
from torch.nn import (AvgPool2d, BatchNorm2d, Conv2d, Linear, MaxPool2d,
                      Module, ReLU, Sequential, Softmax)
import torch.onnx


ActivT = Optional[Callable[[], Module]]

class AverageMeter:
    """Adapted from https://github.com/pytorch/examples/blob/master/imagenet/main.py
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count



def accuracy(output, target, topk=(1, )):
    """Adapted from https://github.com/pytorch/examples/blob/master/imagenet/main.py
    """
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.float().topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.reshape(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].reshape(-1).float().sum(0)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res

def make_conv_pool_activ(
    in_channels: int,
    out_channels: int,
    kernel_size: int,
    activation: ActivT = None,
    pool_size: Optional[int] = None,
    pool_stride: Optional[int] = None,
    **conv_kwargs
):
    layers = [Conv2d(in_channels, out_channels, kernel_size, **conv_kwargs)]
    if activation:
        layers.append(activation())
    if pool_size is not None:
        layers.append(MaxPool2d(pool_size, stride=pool_stride))
    return layers


class Classifier(Module):
    def __init__(
        self, convs: Sequential, linears: Sequential, use_softmax: bool = True
    ):
        super().__init__()
        self.convs = convs
        self.linears = linears
        self.softmax = Softmax(1) if use_softmax else Sequential()

    def forward(self, inputs: torch.Tensor) -> torch.Tensor:
        outputs = self.convs(inputs)
        return self.softmax(self.linears(outputs.view(outputs.shape[0], -1)))


def _make_seq(in_channels, out_channels, c_kernel_size, gc_stride, gc_kernel_size=3):
    return Sequential(
        Conv2d(
            in_channels, 
            in_channels, 
            gc_kernel_size,
            stride=gc_stride,
            groups=in_channels,
            padding=(gc_kernel_size - 1) // 2,
            bias = False),
        BatchNorm2d(in_channels, eps=0.001),
        ReLU(inplace=True),
        Conv2d(
            in_channels,
            out_channels,
            c_kernel_size,
            stride=1,
            bias=False
        ),
        BatchNorm2d(out_channels, eps=0.001),
        ReLU(inplace=True)
    )

def _make_seq1(in_channels, out_channels, c_kernel_size, gc_stride):
    return Sequential(
        Conv2d(
            in_channels,
            out_channels,
            c_kernel_size,
            bias=False,
            stride=gc_stride,
            padding=(c_kernel_size - 1) // 2
        ),
        BatchNorm2d(out_channels, eps=0.001),
        ReLU(inplace=True)
    )

class MobileNet100(Classifier):
    def __init__(self):
        convs = Sequential(
            _make_seq1(1, 32, 1, 2),
            _make_seq(32, 64, 1, 1),
            _make_seq(64, 128, 1, 2),
            _make_seq(128, 128, 1, 1),
            _make_seq(128, 256, 1, 2),
            _make_seq(256, 256, 1, 1),
            _make_seq(256, 512, 1, 2),
            _make_seq(512, 512, 1, 1),
            _make_seq(512, 512, 1, 1),
            _make_seq(512, 512, 1, 1),
            _make_seq(512, 512, 1, 1),
            _make_seq(512, 512, 1, 1),
            _make_seq(512, 1024, 1, 2),
            _make_seq(1024, 1024, 1, 1),
            AvgPool2d(2)
        )
        linears = Sequential(Linear(1024, 8))
        super().__init__(convs, linears)

def model_100(file=None):
    net = MobileNet100().float()
    if file is not None:
        net.load_state_dict(torch.load(file))
    return net

def test_model(nn_model):
    import data

    import PrepareDataset

    batch_size = 32
    test_set = data.SubsetSC("testing")

    labels = sorted(list(set(data[2] for data in test_set)))
    print("Number of labels:", len(labels))
    print(labels)

    waveform, sample_rate, label, spk_id, utt_num = test_set.__getitem__(120)

    print("The sample rate is: ", sample_rate)

    device = "cpu" #torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device)

    def label_to_index(label):
        return torch.tensor(labels.index(label))

    def collate_fn(batch):
        tensors, targets = [], []

        for waveform, _, label, *_ in batch:
            tensors += [waveform]
            targets += [label_to_index(label)]

        tensors = PrepareDataset.preprocess(tensors)
        tensors = torch.stack(tensors)
        targets = torch.stack(targets)

        return tensors, targets

    if device == "cuda":
        num_workers = 1
        pin_memory = True
    else:
        num_workers = 0
        pin_memory = False

    test_loader = torch.utils.data.DataLoader(
        test_set,
        batch_size=batch_size,
        shuffle=False,
        collate_fn=collate_fn,
        num_workers=num_workers,
        pin_memory=pin_memory,
    )

    am1 = AverageMeter()
    am1.reset()

    correct = 0

    for batch_idx, (input, target) in enumerate(test_loader):
        output = nn_model(input)
        _, preds = torch.max(output, 1)
        correct = correct + torch.sum(preds == target)
        prec1, prec5 = accuracy(output.data, target, topk=(1, 5))
        am1.update(prec1.item(), input.size(0))

    print('Accuracy = ',am1.avg)

def to_numpy(tensor):
    return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()

def load_and_test100():
    net = model_100()
    state_dict = torch.load('SC_snn_100.pth')
    net.load_state_dict(state_dict)
    net.eval()
    test_model(net)

load_and_test100()


