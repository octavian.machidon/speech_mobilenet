from scipy import signal
from scipy.fftpack import fft
import librosa
import librosa.display as display
import torch
import numpy as np
import torchvision.transforms as transforms
from PIL import Image
from scipy.io.wavfile import write
import torchaudio
import math

sample_rate = 16000


def get_white_noise(signal,SNR, sr) :
    #RMS value of signal
    RMS_s=math.sqrt(np.mean(signal**2))
    #RMS values of noise
    RMS_n=math.sqrt(RMS_s**2/(pow(10,SNR/10)))
    #Additive white gausian noise. Thereore mean=0
    #Because sample length is large (typically > 40000)
    #we can use the population formula for standard daviation.
    #because mean=0 STD=RMS
    STD_n=RMS_n
    #print(STD_n)

    noise=np.random.normal(0, STD_n, sr)
    return noise

def add_noise(batch, sr=16000):
    new_batch = []
    for data in batch:
        #torchaudio.save("aaa.wav",data, sr)
        noise = get_white_noise(data.numpy(), 1, sr).astype(np.float32)
        data = torch.from_numpy(data.numpy() + noise)
        new_batch += [data]
        #torchaudio.save("bbb.wav",data, sr)
        #sys.exit()        
    return new_batch


def pad_sequence(batch):
    batch = [item.t() for item in batch]
    batch = torch.nn.utils.rnn.pad_sequence(batch, batch_first=True)
  
    return batch.permute(0, 2, 1)


def cal_melspec(batch):
    mel_list = []
    for data in batch:
        # Calculate mel-spectrogram
        S = librosa.feature.melspectrogram(y=data.numpy().squeeze(), sr=sample_rate, n_mels=128)
        # Convert to log scale (dB)
        log_S = librosa.power_to_db(S, ref=np.max)
        # Normalize it
        log_S -= (np.mean(log_S, axis=0) + 1e-8)
        mel_list += [log_S]
    
    return mel_list


def resize(batch):
    batch = [Image.fromarray(data) for data in batch]
    resize = transforms.Resize((64, 64))
    batch = [resize(data) for data in batch]

    cvt_to_tensor = transforms.PILToTensor()
    batch = [cvt_to_tensor(data) for data in batch]

    return batch


def preprocess(batch):
    batch = pad_sequence(batch)
    #batch = add_noise(batch)
    batch = cal_melspec(batch)
    # batch = spec_augment(batch)
    batch = resize(batch)

    return batch

